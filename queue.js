let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    // add an item
    collection.push(item)
    return collection;

}

function dequeue(){
    // remove an item
        collection.shift()
    return collection;
    
}

function front(){
    return collection[0];
    // get the front item

}

function size(){
    // get the size of the array
return collection.length;
}

function isEmpty(){
    // check array if empty
  return collection.length == 0 ;
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};